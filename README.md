# Learning Temporal Behavior Model from Time Series to Detect, Locate, and Explain Anomalies (source code for the experiments)

## Presentation
This repository enable the reproduction of the paper's experiments.

The complete pipeline (data generation, discretization, TA learning, anomaly detection) can be run on the synthetic data.
It is also possible to only run the anomaly detection experiment (or another part of the pipeline), as the folders contain the pre-discretized data and pre-learned models.

To run the experiments on real data, BATADAL and SWaT datasets must be requested and downloaded on the following link beforehand due to datasets terms of usage (SWaT version: A1 & A2_Dec 2015): https://itrust.sutd.edu.sg/itrust-labs_datasets/dataset_info/.
The datasets should be places in their respective _data_ folders, which also contain the script to pre-process them.

## Usage
The experiment was run using Python 3.8.3 and the list of required python packages and their version used are listed in requirements.txt. 
You can install them all at once using the command line `pip3 install -r requirements.txt` in your virtual environment.

Before running the experiment (by **executing main.py**), set the following options in the main.py file:
- Dataset (`string`): synthetic data (synthetic_data), BATADAL, or SWaT
- Discretization method (`string`): MOODES, MOODES+TOPSIS (TOPSIS), Persist, SAX
- Steps to perform (`boolean`): data generation, discretization, TA learning, and/or anomaly detection

⚠️ Please note that if you re-run MOODES, you will have to re-learn the timed automata as the discretization result will be different.
During the code execution, you will be asked if you want to delete the now outdated models.
