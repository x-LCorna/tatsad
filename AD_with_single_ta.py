#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Classes for anomaly detection using a single timed automaton
Author: Lenaig Cornanguer (2023)
"""

import numpy as np
import enum
import TAG


class Detector:
	def __init__(self, tan, data, timed_tolerance=None):
		self.i = 0
		self.tan = tan
		self.data = data
		self.anomalies = list()
		self.timed_tolerance = timed_tolerance # None / 3sigma
		self.replay_detectors = {k: NFTA() for k in tan.keys()}

	def read_data(self):
		events = dict.fromkeys(self.tan.keys())
		row = self.data.iloc[self.i]
		for process in self.tan.keys():
			events[process] = self.tan[process].get_event(row[process])
		return events

	def main(self):
		while self.i < len(self.data):
			data = self.read_data()
			self.update_tan(data)
			self.i += 1

	def update_tan(self, events):
		tanks = [name for name, process in self.tan.items() if isinstance(process, Tank)]
		actuators = [name for name, process in self.tan.items() if isinstance(process, Actuator)]
		isolated_processes = [name for name, process in self.tan.items() if name not in tanks and name not in actuators]
		for name in isolated_processes:
			self.update_isolated_process(name, events)
		for name in tanks:
			self.update_interacting_process(name, events)

	def update_interacting_process(self, name, events):
		tank = self.tan[name]
		event_seq = tank.get_event_seq(events[name])
		intermediate = list()
		for actuator in tank.actuators:
			if events[actuator.name] != actuator.current_event and actuator.current_event is not None:
				synchro = events[actuator.name][:-1] + "!"
				if synchro not in event_seq and tank.allow_synchro(synchro, self.data[name].iloc[self.i]):
					intermediate.append(synchro)
		if len(intermediate) > 0: event_seq = tank.get_event_seq(events[name], intermediate)
		for actuator in tank.actuators:
			if actuator.current_event is None:
				self.update_actuator(actuator.name, events)
			elif actuator.current_event != events[actuator.name] and events[actuator.name][:-1]+"!" not in event_seq:
				cat = Cat.SYMBOLIC if events[actuator.name] in actuator.ta.symbols else Cat.ALPHA
				self.anomalies.append(Anomaly(i=self.i, process=actuator.name, scope=Scope.GLOBAL, cat=cat))
				actuator.reinit()
			elif actuator.current_event == events[actuator.name] and len([s for s in event_seq if actuator.name[2:] in s]) > 0:
				if [s for s in event_seq if actuator.name[2:] in s][0][:-1] != actuator.current_event[:-1]:
					cat = Cat.SYMBOLIC if events[actuator.name] in actuator.ta.symbols else Cat.ALPHA
					self.anomalies.append(Anomaly(i=self.i, process=actuator.name, scope=Scope.GLOBAL, cat=cat))
					actuator.reinit()
				else:
					self.update_actuator(actuator.name, events)
			else:
				self.update_actuator(actuator.name, events)
		self.update_tank(name, event_seq)

	def timed_anomaly(self, edge, process, scope):
		weight = 1
		if self.timed_tolerance == "3sigma":
			guard = [min(edge.reduced_guard()[0], np.mean(edge.guard) - 3 * np.std(edge.guard)),
					 max(edge.reduced_guard()[1], np.mean(edge.guard) + 3 * np.std(edge.guard))]
			anomaly = not (guard[0] < process.clock < guard[1])
		else:
			anomaly = True
		time_delta = self.compute_time_delta(edge, process.clock)
		if anomaly:
			self.anomalies.append(Anomaly(i=self.i, process=process.name, scope=scope,
									  cat=Cat.TEMPORAL, time_delta=time_delta, weight=weight))
			process.reinit()

	def update_tank(self, name, event_seq):
		tank = self.tan[name]
		if tank.current_event is None:
			edge = tank.ta.next_edge(tank.current_state.name, event_seq[0], tank.clock)
			if edge is None:
				edge = tank.ta.next_edge(tank.current_state.name, event_seq[0])
				if edge is None:
					cat = Cat.SYMBOLIC if event_seq[0] in tank.ta.symbols else Cat.ALPHA
					self.anomalies.append(Anomaly(i=self.i, process=name, scope=Scope.LOCAL, cat=cat))
					tank.reinit()
				else: self.timed_anomaly(edge, tank, Scope.LOCAL)
				return
			else:
				self.replay_detectors[name].integrate(event_seq[0], tank.clock)
				if self.replay_detectors[name].is_replay():
					tps = max([v for v in self.replay_detectors[name].possible_states.values()])
					self.anomalies.append(Anomaly(i=self.i, process=name, scope=Scope.LOCAL, cat=Cat.REPLAY, time_delta=tps))
				tank.update(event_seq[0])
			return
		for i in range(1, len(event_seq)):
			edge = tank.ta.next_edge(tank.current_state.name, event_seq[i], tank.clock)
			if edge is None:
				edge = tank.ta.next_edge(tank.current_state.name, event_seq[i])
				if edge is None:
					cat = Cat.SYMBOLIC if event_seq[i] in tank.ta.symbols else Cat.ALPHA
					self.anomalies.append(Anomaly(i=self.i, process=name, scope=Scope.LOCAL, cat=cat))
					tank.reinit()
				else: self.timed_anomaly(edge, tank, Scope.LOCAL)
				return
			else:
				self.replay_detectors[name].integrate(event_seq[i], tank.clock)
				if self.replay_detectors[name].is_replay():
					tps = max([v for v in self.replay_detectors[name].possible_states.values()])
					self.anomalies.append(Anomaly(i=self.i, process=name, scope=Scope.LOCAL, cat=Cat.REPLAY, time_delta=tps))
				tank.update(event_seq[i])
				tank.clock -= 1
		tank.clock += 1

	def update_actuator(self, name, events):
		actuator = self.tan[name]
		if actuator.current_event is not None and events[name][:-1] == actuator.current_event[:-1]:  # No transition
			actuator.clock += 1
		else:  # Transition
			edge = actuator.ta.next_edge(actuator.current_state.name, events[name], actuator.clock)
			if edge is None: # ANOMALY
				edge = actuator.ta.next_edge(actuator.current_state.name, events[name])
				if edge is None:
					cat = Cat.SYMBOLIC if events[name] in actuator.ta.symbols else Cat.ALPHA
					self.anomalies.append(Anomaly(i=self.i, process=name, scope=Scope.GLOBAL, cat=cat))
					actuator.reinit()
				else:
					self.timed_anomaly(edge, actuator, Scope.GLOBAL)
			else: # No anomaly
				self.replay_detectors[name].integrate(events[name], actuator.clock)
				if self.replay_detectors[name].is_replay():
					tps = max([v for v in self.replay_detectors[name].possible_states.values()])
					self.anomalies.append(Anomaly(i=self.i, process=name, scope=Scope.LOCAL, cat=Cat.REPLAY, time_delta=tps))
				actuator.update(events[name])

	def update_isolated_process(self, process, events):
		process = self.tan[process]
		if process.current_event == events[process.name]:
			process.clock += 1
		else:
			edge = process.ta.next_edge(process.current_state.name, events[process.name], process.clock)
			if edge is None:
				edge = process.ta.next_edge(process.current_state.name, events[process.name])
				if edge is None:
					cat = Cat.SYMBOLIC if events[process.name] in process.ta.symbols else Cat.ALPHA
					self.anomalies.append(Anomaly(i=self.i, process=process.name, scope=Scope.LOCAL, cat=cat))
					process.reinit()
				else: self.timed_anomaly(edge, process, Scope.LOCAL)
			else:
				self.replay_detectors[process.name].integrate(events[process.name], process.clock)
				if self.replay_detectors[process.name].is_replay():
					tps = max([v for v in self.replay_detectors[process.name].possible_states.values()])
					self.anomalies.append(Anomaly(i=self.i, process=process.name, scope=Scope.LOCAL,
												  cat=Cat.REPLAY, time_delta=tps))
				process.update(events[process.name])

	def compute_time_delta(self, edge, tv):
		guard = edge.reduced_guard()
		bound = tv > guard[0]
		return tv - guard[int(bound)]

class Scope(enum.Enum):
	LOCAL = 0
	GLOBAL = 1

class Cat(enum.Enum):
	TEMPORAL = 0
	SYMBOLIC = 1
	ALPHA = 4
	UNDEFINED = 2
	REPLAY = 3

class Anomaly:
	def __init__(self, i, process, scope, cat, weight=1, time_delta=0):
		self.i = i
		self.process = process
		self.scope = scope
		self.cat = cat
		self.weight = weight
		self.time_delta = time_delta

class Process:
	def __init__(self, name, ta, bkpts):
		self.name = name
		self.ta = ta
		self.clock = 0
		self.current_state = [s for s in ta.states if s.initial][0]
		self.current_event = None
		self.bkpts = bkpts

	def reinit(self):
		self.current_event = None
		self.clock = 0
		self.current_state = [s for s in self.ta.states if s.initial][0]

	def update(self, event):
		edge = self.ta.next_edge(self.current_state.name, event, self.clock)
		self.current_state = edge.destination
		self.clock = 1
		self.current_event = event

	def get_event(self, value):
		for k, v in self.bkpts.items():
			if v[0] <= value < v[1]:
				return str(k)


class Tank(Process):
	def __init__(self, name, ta, bkpts, actuators, synchros):
		super().__init__(name, ta, bkpts)
		self.actuators = actuators
		self.synchros = synchros

	def get_synchro(self, event, current_event=None):
		if current_event is None: current_event = self.current_event
		for k, v in self.synchros.items():
			if k[0] == current_event and k[1] == event:
				return v["synchro"]

	def allow_synchro(self, synchro, value):
		for k, v in self.synchros.items():
			if synchro == v["synchro"] and v["tol"][0] <= value <= v["tol"][1]:
				return True
		return False

	def get_event_seq(self, event, intermediate=None):
		if self.current_event is None: return [event]
		event_seq = list()
		i_event = self.get_index(event)
		i_current_event = self.get_index(self.current_event)
		if intermediate is None:
			if i_current_event == i_event: return [event]
			indexes = list(range(i_current_event, i_event+self.order(i_current_event, i_event), self.order(i_current_event, i_event)))
			event_seq = self.indexes_to_events(indexes)
		else:
			for synchro in intermediate:
				t = [k for k, v in self.synchros.items() if v['synchro'] == synchro][0]
				i_1 = self.get_index(t[0])
				i_2 = self.get_index(t[1])
				event_seq += self.indexes_to_events(list(range(i_current_event, i_1+self.order(i_current_event, i_1), self.order(i_current_event, i_1))))
				event_seq.append(synchro)
				event_seq += self.indexes_to_events(list(range(i_2, i_event+self.order(i_2, i_event), self.order(i_2, i_event))))
		return event_seq

	def order(self, a, b):
		if a > b: return -1
		elif a <= b: return 1

	def get_index(self, event):
		i = 0
		for k, v in self.bkpts.items():
			if k == event: return i
			i += 1

	def indexes_to_events(self, indexes):
		event_seq = list()
		for i in range(len(indexes)-1):
			event_seq.append(list(self.bkpts.keys())[indexes[i]])
			synchro = self.get_synchro(list(self.bkpts.keys())[indexes[i + 1]], list(self.bkpts.keys())[indexes[i]])
			if synchro is not None: event_seq.append(synchro)
		event_seq.append(list(self.bkpts.keys())[indexes[-1]])
		return event_seq

class Actuator(Process):
	def __init__(self, name, ta, bkpts):
		super().__init__(name, ta, bkpts)

	def get_event(self, value):
		for k, v in self.bkpts.items():
			if value == v: return k

class NFTA:
	def __init__(self):
		self.automaton = TAG.Automaton()
		self.states_queue = list()
		self.possible_states = dict()
		self.i = 2
		self.replay_threshold = 10
		self.queue_limit = 50
		self.no_replay = False

	def del_state(self):
		oldest_state = self.states_queue[0]
		del self.states_queue[0]
		edge = oldest_state.edges_out[0]
		destination = edge.destination
		self.automaton.states.remove(oldest_state)
		self.automaton.edges.remove(edge)
		destination.edges_in.remove(edge)
		if oldest_state in self.possible_states.keys():
			del self.possible_states[oldest_state]

	def integrate(self, symbol, time_value):
		self.no_replay = False
		# initialization
		if len(self.automaton.states) == 1: # S0 created automatically
			self.automaton.add_edge("S0", "S1", symbol, [time_value])
			self.states_queue = [self.automaton.search_state("S0"), self.automaton.search_state("S1")]
			return
		# current paths
		b = len(self.possible_states)
		for state in list(self.possible_states.keys()):
			edge = state.edges_out[0]
			if edge.symbol == symbol and edge.guard[0] == time_value:
				self.possible_states[edge.destination] = self.possible_states[state] + 1
			del self.possible_states[state]
		if b > 0 and len(self.possible_states) == 0:
			self.no_replay = True
		# new paths
		for edge in self.automaton.edges:
			if edge.symbol == symbol and edge.guard[0] == time_value:
				if edge.destination not in self.possible_states.keys():
					self.possible_states[edge.destination] = 1
		# new state at the end
		self.automaton.add_edge(self.states_queue[-1].name, "S" + str(self.i), symbol, [time_value])
		self.states_queue.append(self.automaton.search_state("S" + str(self.i)))
		self.i += 1
		# oldest state deletion
		if len(self.states_queue) >= self.queue_limit: self.del_state()

	def is_replay(self):
		for state, path_length in self.possible_states.items():
			if path_length >= self.replay_threshold:
				return True
		return False
