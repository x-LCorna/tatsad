#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
TA learning with TAG
Author: Lenaig Cornanguer (2023)
"""

import os
import sys

sys.path.append(os.path.dirname(os.getcwd()))

import TAG

def learn_tas(dataset="BATADAL", tss_folder_name="tss_moodes", ta_folder_name="ta_moodes"):
	if dataset == "SWaT":
		raise NotImplementedError("SWaT learning data is located in individual folders per components (TODO).")
	folder = os.getcwd() #os.path.dirname(__file__)
	tss_folder = os.path.join(folder, dataset, tss_folder_name)
	ta_folder = os.path.join(folder, dataset, ta_folder_name)
	for file in os.listdir(tss_folder):
		if "ta_"+file[4:] in os.listdir(ta_folder): continue # To not relearn a TA in case of interruption
		with open(os.path.join(folder, dataset, ta_folder_name, "ta_"+file[4:]), 'w') as f:
			f.write("KO") # To allow running this script multiple time in parallel
		l = TAG.TALearner(tss_path=os.path.join(tss_folder, file))
		mem = l.ta.print(reduced_guard=False, gtime=False)

		with open(os.path.join(folder, dataset, ta_folder_name, "ta_"+file[4:]), 'w') as f:
			f.write('\n'.join(mem))

