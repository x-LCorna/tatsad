#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Anomaly detection functions
Author: Lenaig Cornanguer (2023)
"""

import os
from datetime import datetime

import numpy as np
import pandas as pd
from sklearn.metrics import precision_recall_fscore_support

from options import Discretization

def import_classes(discretization):
    if discretization == Discretization.MOODES:
        from BATADAL.initialization import import_tan_mood as import_tan
        from AD_with_ensemble import Detector, Scope, Cat, Anomaly, Process, Tank, Actuator, Process, NFTA
    else:
        from AD_with_single_ta import Detector, Scope, Cat, Anomaly, Process, Tank, Actuator, Process, NFTA
        if discretization == Discretization.SAX:
            from SAX.sax_initialization import import_tan_sax as import_tan
        elif discretization == Discretization.PERSIST:
            from Persist.persist_initialization import import_tan_persist as import_tan
        else: # elif discretization == Discretization.TOPSIS:
            from BATADAL.topsis_initialization import import_tan_topsis as import_tan
    return import_tan, Detector, Scope, Cat, Anomaly, Process, Tank, Actuator, NFTA

def anomaly_detection_swat(timed_tolerance="3sigma", w_size=10):
    discretization = Discretization.MOODES
    _, Detector, _, _, _, _, _, _, _ = import_classes(discretization)
    from SWaT.initialization_swat import import_tan_mood

    #### DATA IMPORTATION ####
    path = os.path.dirname(__file__)
    datafolder = os.path.join(path, "SWaT", "data", "Physical")
    filename = "SWaT_Dataset_Attack_v0.csv"
    parser = lambda x: datetime.strptime(x, '%d/%m/%Y %I:%M:%S %p')
    data_test = pd.read_csv(os.path.join(datafolder, filename), sep=";", decimal=",", parse_dates=['Timestamp'], date_parser=parser, index_col=0)
    truth_test = np.array(data_test[["0/1"]])
    data_test.drop(["0/1", 'Timestamp'], axis=1, inplace=True)
    data_calib = pd.read_csv(os.path.join(datafolder, "SWaT_Dataset_Normal_v1.csv"), sep=";", decimal=",")
    data_calib.drop(["0/1", 'Timestamp'], axis=1, inplace=True)
    data_calib = data_calib[400000:]
    groups = {c: [c] for c in data_test.columns}

    #### CALIBRATION ####
    tan = import_tan_mood()

    detector_calib = Detector(tan, data_calib, timed_tolerance=timed_tolerance)
    detector_calib.main()
    max_per_groups, alert_score_per_group = calibration(detector_calib.anomalies.copy(), len(data_calib), groups, w_size, excluded_var=[],
                                 ablation_no_actu_no_tank=False, ablation_no_replay=True, discretization=discretization, lim=False)

    excluded_var = [g for g in groups if (g[:3] == "AIT" or (g[:1] == "P" and g[:3] != "PIT")) or sum(alert_score_per_group[g]) >= 1000]

    #### ANOMALY DETECTION ####
    detector_test = Detector(tan, data_test, timed_tolerance="3sigma")
    detector_test.main()

    #### ANALYSIS ####
    analysis(detector_test.anomalies.copy(), len(data_test), truth_test, max_per_groups, groups,
             w_size, excluded_var=excluded_var, soft=True, ablation_no_actu_no_tank=False, ablation_no_replay=True,
             discretization=discretization, lim=False)

def anomaly_detection_synthetic(setup="noise_01", timed_tolerance="3sigma", w_size=7):
    discretization = Discretization.MOODES
    if setup=="noise_0": timed_tolerance = "None"

    _, Detector, _, _, _, _, _, _, _ = import_classes(discretization)
    from synthetic_data.initialization import import_tan_mood

    #### DATA IMPORTATION ####
    path = os.path.dirname(__file__)
    data_calib = pd.read_csv(os.path.join(path, "synthetic_data", setup, "data", "calib.csv"), sep=",", decimal=".")
    data_test = pd.read_csv(os.path.join(path, "synthetic_data", setup, "data", "test.csv"), sep=",", decimal=".")
    truth_test = pd.read_csv(os.path.join(path, "synthetic_data", setup, "data", "truth.csv"), engine='python')
    truth_test.rename(columns=lambda x: "ATT_FLAG", inplace=True)
    groups = {g: [g] for g in data_test.columns}

    #### CALIBRATION ####
    tan = import_tan_mood(setup)
    detector_calib = Detector(tan, data_calib, timed_tolerance=timed_tolerance)
    detector_calib.main()
    max_per_groups, _ = calibration(detector_calib.anomalies.copy(), len(data_calib), groups, w_size, excluded_var=[], ablation_no_actu_no_tank=False, ablation_no_replay=True, discretization=discretization)

    #### ANOMALY DETECTION ####
    tan = import_tan_mood(setup)
    detector_test = Detector(tan, data_test, timed_tolerance)
    detector_test.main()

    #### ANALYSIS ####
    analysis(detector_test.anomalies.copy(), len(data_test), np.copy(truth_test.ATT_FLAG), max_per_groups, groups, w_size, excluded_var=[], soft=True, ablation_no_actu_no_tank=False, ablation_no_replay=True, discretization=discretization)

def anomaly_detection_batadal(discretization=Discretization.MOODES, w_size=10, timed_tolerance="3sigma", ablation_no_actu_no_tank=False, ablation_no_replay=False):
    import_tan, Detector, _, _, _, _, _, _, _ = import_classes(discretization)

    #### DATA IMPORTATION ####
    path = os.path.dirname(__file__)
    data04 = pd.read_csv(os.path.join(path, "BATADAL", "data", "BATADAL_dataset04.csv"), dtype={'DATETIME': str}, sep=';', engine='python')
    data_test = pd.read_csv(os.path.join(path, "BATADAL", "data", "BATADAL_test_dataset.csv"), dtype={'DATETIME': str}, sep=';', engine='python')
    truth_test = pd.read_csv(os.path.join(path, "BATADAL", "data", "BATADAL_test_dataset_groundtruth.csv"), dtype={'DATETIME': str}, sep=';', engine='python')

    excluded_var = ["P_J280", "F_PU3", "F_PU5", "F_PU9"]

    groups = dict()
    for process in ['L_T1', 'L_T2', 'L_T3', 'L_T4', 'L_T5', 'L_T6', 'L_T7', 'F_PU1', 'S_PU1', 'F_V2', 'S_V2', 'F_PU2',
                    'S_PU2', 'F_PU4', 'S_PU4', 'F_PU5', 'S_PU5', 'F_PU6', 'S_PU6', 'F_PU7', 'S_PU7', 'F_PU8', 'S_PU8',
                    'F_PU9', 'S_PU9', 'F_PU10', 'S_PU10', 'F_PU11', 'S_PU11', 'P_J280', 'P_J269', 'P_J422', 'P_J14',
                    'P_J317', 'P_J307', 'P_J306', 'P_J302', 'P_J415', 'P_J289', 'P_J302', 'P_J256', 'P_J300']:
        if process[2:] not in groups.keys():
            groups[process[2:]] = [process]
        else:
            groups[process[2:]].append(process)

    #### TAN IMPORTATION ####
    tan = import_tan()

    #### CALIBRATION ####
    detector04 = Detector(tan, data04, timed_tolerance)
    detector04.main()
    max_per_groups, _ = calibration(detector04.anomalies.copy(), len(data04), groups, w_size, excluded_var, ablation_no_actu_no_tank, ablation_no_replay, discretization, keep=1000)

    #### ANOMALY DETECTION ####
    detector_test = Detector(tan, data_test, timed_tolerance)
    detector_test.main()

    #### ANALYSIS ####
    analysis(detector_test.anomalies.copy(), len(data_test), np.copy(truth_test.ATT_FLAG), max_per_groups, groups, w_size, excluded_var, discretization, ablation_no_actu_no_tank, ablation_no_replay)


def compute_scores(anomalies, n, groups, w_size, excluded_var, ablation_no_actu_no_tank, ablation_no_replay, discretization, lim):
    anomalies = ablation_preprocessing(anomalies, ablation_no_actu_no_tank, ablation_no_replay, discretization)

    # Scores
    anomalies, alert_score_per_group = anomaly_score(anomalies, n, groups, excluded_var, discretization, lim) # AS
    cudas_per_group = dict.fromkeys(groups.keys())
    for g in groups:
        cudas_per_group[g] = CuDAS(alert_score_per_group[g], w_size) # CuDAS

    return alert_score_per_group, cudas_per_group, anomalies

def calibration(anomalies, n, groups, w_size, excluded_var, ablation_no_actu_no_tank, ablation_no_replay, discretization, lim=True, keep=None):
    alert_score_per_group, cudas_per_group, anomalies = compute_scores(anomalies, n, groups, w_size, excluded_var, ablation_no_actu_no_tank, ablation_no_replay, discretization, lim)
    if keep is None: keep = n
    max_per_groups = dict.fromkeys(groups.keys())
    for group in groups:
        t = np.quantile(cudas_per_group[group][:keep], 1, method="higher")
        max_per_groups[group] = t

    return max_per_groups, alert_score_per_group

def anomaly_score(anomalies, n, groups, excluded_var, discretization, lim):
    _, _, Scope, Cat, Anomaly, _, _, _, _ = import_classes(discretization)
    alert_score_per_group = dict.fromkeys(groups.keys())
    for group in groups:
        alert_score_per_group[group] = np.zeros(shape=(n))

    reserve = list()
    for a in anomalies:
        if a.process in excluded_var: continue # No model learned or abnormal data (P_J280)
        if a.cat == Cat.REPLAY: continue # replay anomalies not taken into account for CuDAS because 0/1 value
        gg = [k for k, v in groups.items() if a.process in v]
        for g in gg: alert_score_per_group[g][a.i] += a.weight
        if (0 < a.time_delta < 48) or not lim: # anomaly detected since upper guard value exceeded
            for t in range(a.i - a.time_delta, a.i):
                for g in gg: alert_score_per_group[g][t] += a.weight
                reserve.append(Anomaly(i=t, process=a.process, scope=Scope.LOCAL, cat=Cat.TEMPORAL, time_delta=a.time_delta, weight=a.weight))
    anomalies += reserve
    return anomalies, alert_score_per_group

def pred_from_scores(cudas_per_group, max_per_groups, n, soft=False):
    predicted = np.zeros(shape=(n))
    predicted_per_groups = dict.fromkeys(max_per_groups.keys())
    for group in max_per_groups.keys():
        predicted_per_groups[group] = np.zeros(shape=(n))
        for i in range(n):
            if (not soft and cudas_per_group[group][i] >= max_per_groups[group] and max_per_groups[group] > 0) or (soft and cudas_per_group[group][i] > max_per_groups[group]):
                predicted[i] = 1
                predicted_per_groups[group][i] = 1
    return predicted, predicted_per_groups

def pred_from_replay(anomalies, n, groups, excluded_var, discretization, ablation_no_replay):
    _, _, _, Cat, _, _, _, _, _ = import_classes(discretization)
    predicted_replay = np.zeros(shape=(n))
    predicted_per_groups_replay = dict.fromkeys(groups.keys())
    for group in groups:
        predicted_per_groups_replay[group] = np.zeros(shape=(n))
    if ablation_no_replay: return predicted_replay, predicted_per_groups_replay
    for a in anomalies:
        if a.process in excluded_var: continue
        if a.process[:2] in ["S_", "L_"]: continue # Replay module only for actuator pressure and flow
        if a.cat == Cat.REPLAY:
            group = [k for k, v in groups.items() if a.process in v][0]
            predicted_per_groups_replay[group][a.i] = 1
            predicted_replay[a.i] = 1
    return predicted_replay, predicted_per_groups_replay

def safe_div(x, y):
    return 0 if y == 0 else x / y

def CuDAS(alert_score, w_size):
    cudas = np.zeros(shape=alert_score.shape)
    tmp = np.zeros(shape=alert_score.shape)
    for t in range(len(cudas)):
        tmp[t] = alert_score[t]
        for tt in range(max(0, t - w_size), t):
            tmp[t] += alert_score[tt] / (t - tt + 1)
        cudas[t] = tmp[t]
    return cudas

def ablation_preprocessing(anomalies, ablation_no_actu_no_tank, ablation_no_replay, discretization):
    _, _, _, Cat, _, _, _, _, _ = import_classes(discretization)
    if ablation_no_actu_no_tank:
        to_remove = list()
        for a in anomalies:
            if a.process[:2] in ["S_", "L_"]:
                to_remove.append(a)
        anomalies = list(set(anomalies)-set(to_remove))
    if ablation_no_replay:
        to_remove = list()
        for a in anomalies:
            if a.cat == Cat.REPLAY:
                to_remove.append(a)
        anomalies = list(set(anomalies)-set(to_remove))
    return anomalies

def analysis(anomalies, n, truth, max_per_groups, groups, w_size, excluded_var, discretization, soft=False, ablation_no_replay=True, ablation_no_actu_no_tank=False, detailed_analysis=False, lim=True):
    alert_score_per_group, cudas_per_group, anomalies = compute_scores(anomalies, n, groups, w_size, excluded_var, ablation_no_actu_no_tank, ablation_no_replay, discretization, lim)
    predicted, predicted_per_groups = pred_from_scores(cudas_per_group, max_per_groups, n, soft)
    predicted_replay, predicted_per_groups_replay = pred_from_replay(anomalies, n, groups, excluded_var, discretization, ablation_no_replay)

    precision, recall, fscore, _ = precision_recall_fscore_support(truth, np.logical_or(predicted, predicted_replay).astype(int), average='binary')
    print(f'TPR: {recall:.3f}, PPV: {precision:.3f}, F1: {fscore:.3f}')

    if detailed_analysis:
        res = analysis_batadal_detailed(anomalies, truth, predicted_per_groups, predicted_per_groups_replay, cudas_per_group, groups, excluded_var, discretization)


def analysis_batadal_detailed(anomalies, truth, predicted_per_groups, predicted_per_groups_replay, cudas_per_group, groups, excluded_var, discretization):
    _, _, Scope, Cat, _, _, _, _, _ = import_classes(discretization)

    attacks = dict()
    attacks[1] = {"dur": (297, 366), "anomalies": list(), "processes": dict(), "cudas": dict(), "scope": {Scope.LOCAL: 0, Scope.GLOBAL: 0}, "type": {Cat.REPLAY: 0, Cat.TEMPORAL: 0, Cat.SYMBOLIC: 0, Cat.ALPHA_LOW: 0, Cat.ALPHA_HIGH: 0}, "groups": dict()}
    attacks[2] = {"dur": (632, 696), "anomalies": list(), "processes": dict(), "cudas": dict(), "scope": {Scope.LOCAL: 0, Scope.GLOBAL: 0}, "type": {Cat.REPLAY: 0, Cat.TEMPORAL: 0, Cat.SYMBOLIC: 0, Cat.ALPHA_LOW: 0, Cat.ALPHA_HIGH: 0}, "groups": dict()}
    attacks[3] = {"dur": (867, 897), "anomalies": list(), "processes": dict(), "cudas": dict(), "scope": {Scope.LOCAL: 0, Scope.GLOBAL: 0}, "type": {Cat.REPLAY: 0, Cat.TEMPORAL: 0, Cat.SYMBOLIC: 0, Cat.ALPHA_LOW: 0, Cat.ALPHA_HIGH: 0}, "groups": dict()}
    attacks[4] = {"dur": (937, 967), "anomalies": list(), "processes": dict(), "cudas": dict(), "scope": {Scope.LOCAL: 0, Scope.GLOBAL: 0}, "type": {Cat.REPLAY: 0, Cat.TEMPORAL: 0, Cat.SYMBOLIC: 0, Cat.ALPHA_LOW: 0, Cat.ALPHA_HIGH: 0}, "groups": dict()}
    attacks[5] = {"dur": (1229, 1328), "anomalies": list(), "processes": dict(), "cudas": dict(), "scope": {Scope.LOCAL: 0, Scope.GLOBAL: 0}, "type": {Cat.REPLAY: 0, Cat.TEMPORAL: 0, Cat.SYMBOLIC: 0, Cat.ALPHA_LOW: 0, Cat.ALPHA_HIGH: 0}, "groups": dict()}
    attacks[6] = {"dur": (1574, 1653), "anomalies": list(), "processes": dict(), "cudas": dict(), "scope": {Scope.LOCAL: 0, Scope.GLOBAL: 0}, "type": {Cat.REPLAY: 0, Cat.TEMPORAL: 0, Cat.SYMBOLIC: 0, Cat.ALPHA_LOW: 0, Cat.ALPHA_HIGH: 0}, "groups": dict()}
    attacks[7] = {"dur": (1940, 1969), "anomalies": list(), "processes": dict(), "cudas": dict(), "scope": {Scope.LOCAL: 0, Scope.GLOBAL: 0}, "type": {Cat.REPLAY: 0, Cat.TEMPORAL: 0, Cat.SYMBOLIC: 0, Cat.ALPHA_LOW: 0, Cat.ALPHA_HIGH: 0}, "groups": dict()}

    for k, d in attacks.items():
        for i in range(d["dur"][0], d["dur"][1]+1):
            for g, p in groups.items():
                if predicted_per_groups[g][i] > 0 or predicted_per_groups_replay[g][i] > 0:
                    if p[0] not in attacks[k]["groups"].keys(): attacks[k]["groups"][p[0]] = 0
                    attacks[k]["groups"][p[0]] += 1
    for a in anomalies:
        if a.process in excluded_var: continue
        group = [k for k, v in groups.items() if a.process in v][0]
        if not truth[a.i] or (a.cat != Cat.REPLAY and not predicted_per_groups[group][a.i]): continue
        if a.cat == Cat.REPLAY and a.process[:2] in ["S_", "L_"]: continue
        attack = [k for k, v in attacks.items() if v["dur"][0] <= a.i <= v["dur"][1]][0]
        attacks[attack]["anomalies"].append(a)
        if a.process not in attacks[attack]["processes"]: attacks[attack]["processes"][a.process] = 0
        attacks[attack]["processes"][a.process] += a.weight
        attacks[attack]["scope"][a.scope] += a.weight
        attacks[attack]["type"][a.cat] += a.weight

    for attack in attacks.keys():
        for i in range(attacks[attack]["dur"][0], attacks[attack]["dur"][1]+1):
            for name, group in groups.items():
                if predicted_per_groups[name][i]:
                    if group[0] not in attacks[attack]["cudas"].keys(): attacks[attack]["cudas"][group[0]] = 0
                    attacks[attack]["cudas"][group[0]] += cudas_per_group[name][i]
        for a in attacks[attack]["anomalies"]:
            if a.cat == Cat.REPLAY:
                if a.process not in attacks[attack]["cudas"].keys(): attacks[attack]["cudas"][a.process] = 0
                attacks[attack]["cudas"][a.process] += a.weight

    for k in attacks.keys():
        dico = attacks[k]["anomalies"]
        attacks[k]["processes"] = dict.fromkeys({a.process for a in dico})
        for a in dico:
            if attacks[k]["processes"][a.process] is None: attacks[k]["processes"][a.process] = 0
            attacks[k]["processes"][a.process] += a.weight

    res_final = pd.DataFrame(columns=["attack", "process", "%", "symb", "alpha_low", "alpha_high", "temp_low", "temp_high", "replay"])
    for attack in attacks.keys():
        for process in attacks[attack]["groups"].keys():
            replay = sum([a.weight for a in attacks[attack]["anomalies"] if a.process == process and a.cat == Cat.REPLAY])
            temp_low = sum([a.weight for a in attacks[attack]["anomalies"] if a.process == process and a.cat == Cat.TEMPORAL and a.time_delta < 0])
            temp_high = sum([a.weight for a in attacks[attack]["anomalies"] if a.process == process and a.cat == Cat.TEMPORAL and a.time_delta > 0])
            symb = sum([a.weight for a in attacks[attack]["anomalies"] if a.process == process and a.cat == Cat.SYMBOLIC])
            alpha_low = sum([a.weight for a in attacks[attack]["anomalies"] if a.process == process and a.cat == Cat.ALPHA_LOW])
            alpha_high = sum([a.weight for a in attacks[attack]["anomalies"] if a.process == process and a.cat == Cat.ALPHA_HIGH])
            tot = replay + temp_low + temp_high + symb + alpha_low + alpha_high
            pourc = attacks[attack]["cudas"][process] / sum(attacks[attack]["cudas"].values())
            new_row = {"attack": attack, "process": process, "%": pourc, "symb": safe_div(symb, tot),
                       "alpha_low": safe_div(alpha_low, tot),
                       "alpha_high": safe_div(alpha_high, tot), "temp_low": safe_div(temp_low, tot),
                       "temp_high": safe_div(temp_high, tot), "replay": safe_div(replay, tot)}
            res_final = pd.concat([res_final, pd.DataFrame([new_row])], ignore_index=True)
    res_final = res_final.sort_values(by=["attack", "%"], ascending=[True, False])

    return res_final
