#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Persist discretization & TA learning
Author: Lénaïg Cornanguer (2023)
"""

import os
import numpy as np
import pandas as pd

from Persist.Persist import Persist
from utils_tss import cut_into_seq, summarize
import TAG

def discretize_with_persist(options):
	path = os.getcwd()
	data = pd.read_csv(os.path.join(path, "BATADAL", "data", "BATADAL_dataset03.csv"), dtype={'DATETIME': str}, sep=',', engine='python')
	res_df = pd.DataFrame(columns=["Process", "Breakpoints"])

	# DISCRETIZATION
	if options.discretization:
		print('Data discretization...')
		for process in [c for c in data.columns if c[:2] == "F_" or c[:2] == "P_"]:
			ts = np.array(data[process])
			p = Persist(ts.reshape(1, -1), kmin=2, kmax=20, candidates='EF')
			bins = p.bins
			if process not in ["P_J415", "F_PU10", "P_J302", "P_J307"]:
				bkpts_persist = bins[np.argmax(p.pscores)]
			else:
				bkpts_persist = bins[np.argsort(p.pscores, axis=0)[-2][0]] # second greater
			s = np.zeros(shape=ts.shape)
			breakpoints = np.concatenate(
				(-np.inf, np.sort([b for b in bkpts_persist if not np.isnan(b)]), np.inf), axis=None)
			for i in range(len(breakpoints) - 1):
				s[np.nonzero(np.logical_and(ts >= breakpoints[i], ts < breakpoints[i + 1]))] = int(i)
			tss_list = [" ".join(e) for e in cut_into_seq(summarize(s), 10)]
			with open(os.path.join(path, "Persist", "tss_persist", "tss_"+process+"_persist.txt"), "w") as f:
				f.write('\n'.join(tss_list))
			res_df = pd.concat([res_df, pd.DataFrame({"Process": process, "Breakpoints": [breakpoints]})], ignore_index=True)
		res_df.to_csv(os.path.join(path, "Persist", "persist_results.csv"))

	# TA LEARNING
	if options.ta_learning:
		print('TA learning...')
		res_df = pd.read_csv(os.path.join(path, "Persist", "persist_results.csv"), sep=',', engine='python')
		for process in ['F_PU10', 'F_PU11', 'F_V2', 'P_J280', 'P_J269', 'P_J300', 'P_J256', 'P_J289', 'P_J415', 'P_J302', 'P_J306', 'P_J307', 'P_J317', 'P_J14', 'P_J422']:
			l = TAG.TALearner(tss_path=os.path.join(path, "Persist", "tss_persist", "tss_" + process + "_persist.txt"))
			mem = l.ta.print(reduced_guard=False, gtime=False)

			with open(os.path.join(path, "Persist", "ta_persist", "ta_persist_"+process+".txt"), 'w') as f:
				f.write('\n'.join(mem))
