#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Time series discretization for BATADAL (discretized by MOODES)
Author: Lenaig Cornanguer (2023)
"""

import os
import pandas as pd
from AD_with_ensemble import *

def import_tan_mood():
    folder = os.getcwd()
    data = pd.read_csv(os.path.join(folder, "BATADAL", "data", "BATADAL_dataset03.csv"), dtype={'DATETIME':str})
    res_df = pd.read_csv(os.path.join(folder, "BATADAL", "moodes_results.csv"), sep=",", index_col=0)

    tas = dict()
    tan = dict.fromkeys(['S_PU1', 'S_PU2', 'S_PU3', 'S_PU4', 'S_PU5', 'S_PU6',
                         'S_PU7', 'S_PU8', 'S_PU9', 'S_PU10', 'S_PU11', 'S_V2',
                         'L_T1', 'L_T2', 'L_T3', 'L_T4', 'L_T5', 'L_T7', 'F_PU1',
                         'F_PU2', 'F_PU4', 'F_PU6', 'F_PU7', 'F_PU8', 'F_PU10',
                         'F_PU11', 'F_V2', 'P_J280', 'P_J269', 'P_J300', 'P_J256',
                         'P_J289', 'P_J415', 'P_J302', 'P_J306', 'P_J307', 'P_J317',
                         'P_J14', 'P_J422'])
    for process in tan.keys():
        if process[:2] in ["P_", "F_"]:
            tan[process] = Ensemble(name=process)
            subset = res_df.loc[(res_df['Process'] == process)]
            for l, row in subset.iterrows():
                i = l - min(res_df.loc[(res_df['Process'] == process)].index)
                filename = os.path.join(folder, "BATADAL", "ta_moodes", "ta_" + process +"_"+str(i)+".txt")
                if not os.path.exists(filename): continue
                ta = TAG.Automaton(dot_path=filename)
                bkpts = dict()
                breakpoints = np.concatenate((-np.inf, min(data[process]), eval(row.Breakpoints), max(data[process]), np.inf), axis=None)
                for b in range(len(breakpoints) - 1):
                    bkpts[str(b)] = [breakpoints[b], breakpoints[b + 1]]
                p = Process(name=row.Process,
                            ta=ta,
                            bkpts=bkpts)
                tan[process].automata[i] = p
        else:
            filename = os.path.join(folder, "BATADAL", "ta_moodes", "ta_"+process+".txt")
            ta = TAG.Automaton(dot_path=filename)
            tas[process] = ta

    #### Knowledge given for BATADAL challenge (synchros between tanks and actuators ####
    s_v2 = Actuator(name="S_V2",
                    ta=tas["S_V2"],
                    bkpts={"openV2?": 1, "closeV2?": 0})
    s_pu1 = Actuator(name="S_PU1",
                     ta=tas["S_PU1"],
                     bkpts={"openPU1?": 1, "closePU1?": 0})
    s_pu2 = Actuator(name="S_PU2",
                     ta=tas["S_PU2"],
                     bkpts={"openPU2?": 1, "closePU2?": 0})
    s_pu3 = Actuator(name="S_PU3",
                     ta=tas["S_PU3"],
                     bkpts={"openPU3?": 1, "closePU3?": 0})
    s_pu4 = Actuator(name="S_PU4",
                     ta=tas["S_PU4"],
                     bkpts={"openPU4?": 1, "closePU4?": 0})
    s_pu5 = Actuator(name="S_PU5",
                     ta=tas["S_PU5"],
                     bkpts={"openPU5?": 1, "closePU5?": 0})
    s_pu6 = Actuator(name="S_PU6",
                     ta=tas["S_PU6"],
                     bkpts={"openPU6?": 1, "closePU6?": 0})
    s_pu7 = Actuator(name="S_PU7",
                     ta=tas["S_PU7"],
                     bkpts={"openPU7?": 1, "closePU7?": 0})
    s_pu8 = Actuator(name="S_PU8",
                     ta=tas["S_PU8"],
                     bkpts={"openPU8?": 1, "closePU8?": 0})
    s_pu9 = Actuator(name="S_PU9",
                     ta=tas["S_PU9"],
                     bkpts={"openPU9?": 1, "closePU9?": 0})
    s_pu10 = Actuator(name="S_PU10",
                      ta=tas["S_PU10"],
                      bkpts={"openPU10?": 1, "closePU10?": 0})
    s_pu11 = Actuator(name="S_PU11",
                      ta=tas["S_PU11"],
                      bkpts={"openPU11?": 1, "closePU11?": 0})

    l_t1 = Tank(name="L_T1",
                ta=tas["L_T1"],
                bkpts={"1": [0, 0.320111841], "2": [0.320111841, 1], "3": [1, 4], "4": [4, 4.5], "5": [4.5, 6.3],
                       "6": [6.3, 6.51], "7": [6.51, np.inf]},
                actuators=[s_pu1, s_pu2],
                synchros={("4", "3"): {"synchro": "openPU1!", "tol": [0.509729922, 0.509729922]}, # min and max observed values
                          ("5", "6"): {"synchro": "closePU1!", "tol": [0, 0]},
                          ("3", "2"): {"synchro": "openPU2!", "tol": [0.509729922, 1.117213607]},
                          ("4", "5"): {"synchro": "closePU2!", "tol": [4.237445354, 4.66071701]}})

    l_t2 = Tank(name="L_T2",
                ta=tas["L_T2"],
                bkpts={"1": [0, 0.293875813], "2": [0.293875813, 0.5], "3": [0.5, 5.5], "4": [5.5, 5.663802147],
                       "5": [5.663802147, 5.91], "6": [5.91, np.inf]},
                actuators=[s_v2],
                synchros={("3", "4"): {"synchro": "closeV2!", "tol": [4.710795403, 5.663802147]},
                          ("3", "2"): {"synchro": "openV2!", "tol": [0.293875813, 2.049002886]}})

    l_t3 = Tank(name="L_T3",
                ta=tas["L_T3"],
                bkpts={"1": [0, 1], "2": [1, 3], "3": [3, 3.5], "4": [3.5, 5.3], "5": [5.3, 5.433021545],
                       "6": [5.433021545, 6.76], "7": [6.76, np.inf]},
                actuators=[s_pu4, s_pu5],
                synchros={("3", "2"): {"synchro": "openPU4!", "tol": [2.882846594, 3.501587152]},
                          ("4", "5"): {"synchro": "closePU4!", "tol": [4.848326206, 5.433021545]},
                          ("2", "1"): {"synchro": "openPU5!", "tol": [0, 0]},
                          ("3", "4"): {"synchro": "closePU5!", "tol": [3.191145182, 3.191145182]}})

    l_t4 = Tank(name="L_T4",
                ta=tas["L_T4"],
                bkpts={"1": [0, 2], "2": [2, 3], "3": [3, 3.5], "4": [3.5, 4.5], "5": [4.5, 4.71], "6": [4.71, np.inf]},
                actuators=[s_pu6, s_pu7],
                synchros={("2", "1"): {"synchro": "openPU6!", "tol": [2.006145, 2.510221481]},
                          ("3", "4"): {"synchro": "closePU6!", "tol": [2.792634249, 3.700614214]},
                          ("3", "2"): {"synchro": "openPU7!", "tol": [2.403930187, 4.021317959]},
                          ("4", "5"): {"synchro": "closePU7!", "tol": [3.009527445, 4.690665245]}})

    l_t5 = Tank(name="L_T5",
                ta=tas["L_T5"],
                bkpts={"1": [0, 1.293241262], "2": [1.293241262, 1.5], "3": [1.5, 4], "4": [4, 4.162725925],
                       "5": [4.162725925, 4.51], "6": [4.51, np.inf]},
                actuators=[s_pu8],
                synchros={("3", "2"): {"synchro": "openPU8!", "tol": [1.293241262, 2.656090975]},
                          ("3", "4"): {"synchro": "closePU8!", "tol": [3.189588547, 4.162725925]}})

    l_t7 = Tank(name="L_T7",
                ta=tas["L_T7"],
                bkpts={"1": [0, 1], "2": [1, 2.5], "3": [2.5, 3], "4": [3, 4.8], "5": [4.8, 4.999618053],
                       "6": [4.999618053, 5.01], "7": [5.01, np.inf]},
                actuators=[s_pu10, s_pu11],
                synchros={("3", "2"): {"synchro": "openPU10!", "tol": [1.560883403, 3.669429064]},
                          ("4", "5"): {"synchro": "closePU10!", "tol": [2.862300158, 4.999618053]},
                          ("2", "1"): {"synchro": "openPU11!", "tol": [1.175509572, 1.227926254]},
                          ("3", "4"): {"synchro": "closePU11!", "tol": [1.562320828, 4.055660725]}})

    tans = {"S_PU1": s_pu1, "S_PU2": s_pu2, "S_PU3": s_pu3, "S_PU4": s_pu4, "S_PU5": s_pu5, "S_PU6": s_pu6,
           "S_PU7": s_pu7, "S_PU8": s_pu8, "S_PU9": s_pu9, "S_PU10": s_pu10, "S_PU11": s_pu11, "S_V2": s_v2,
           "L_T1": l_t1, "L_T2": l_t2, "L_T3": l_t3, "L_T4": l_t4, "L_T5": l_t5, "L_T7": l_t7}

    for k, v in tans.items():
        tan[k] = v
    return tan
