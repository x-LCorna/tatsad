# -*- coding: utf-8 -*-
# http://semver.org/
import sys

from .TALearner import *

__all__ = [
    'TALearner', 'Automaton',
    'Edge', 'State'
]