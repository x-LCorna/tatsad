#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Main file to execute to launch the experiment
Author: Lenaig Cornanguer (2023)
"""

from options import Options
from run_experiments import run

if __name__ == '__main__':
    options = Options(dataset='synthetic_data', # [synthetic_data/BATADAL/SWaT]
                      discretization_method='MOODES', # [MOODES/TOPSIS/Persist/SAX]
                      data_generation=False, # only relevant for synthetic data
                      discretization=False,
                      ta_learning=False,
                      anomaly_detection=True)

    run(options)

