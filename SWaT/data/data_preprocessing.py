#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SWaT data pre-processing
Author: Lenaig Cornanguer (2023)
"""

import os
import pandas as pd

path = os.getcwd()

normal = pd.read_excel(os.path.join(path, 'Physical', 'SWaT_Dataset_Normal_v1.xlsx'), skiprows=0, header=1)
normal.rename(columns=lambda x: x.strip(), inplace=True)
normal['0/1'] = normal['Normal/Attack'].apply(lambda x: 0 if x == 'Normal' else 1)
normal.drop(['Normal/Attack'], axis=1, inplace=True)
normal.to_csv(os.path.join(path, 'Physical', 'SWaT_Dataset_Normal_v1.csv'), sep=';', decimal=',')

attack = pd.read_excel(os.path.join(path, 'Physical', 'SWaT_Dataset_Attack_v0.xlsx'), skiprows=0, header=1)
attack.rename(columns=lambda x: x.strip(), inplace=True)
attack['Timestamp'] = attack['Timestamp'].apply(lambda x: x.strip())
attack['0/1'] = attack['Normal/Attack'].apply(lambda x: 0 if x == 'Normal' else 1)
attack.drop(['Normal/Attack'], axis=1, inplace=True)
attack.to_csv(os.path.join(path, 'Physical', 'SWaT_Dataset_Attack_v0.csv'), sep=';', decimal=',')
