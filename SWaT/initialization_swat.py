#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Time series discretization for SWaT
Author: Lenaig Cornanguer (2023)
"""

import os
import pandas as pd
from AD_with_ensemble import *
import TAG

def import_tan_mood():
    folder = os.getcwd()
    data = pd.read_csv(os.path.join(folder, 'SWaT', 'data', 'Physical', 'SWaT_Dataset_Attack_v0.csv'), sep=";", decimal=",")
    data = data[100000:400000]
    data.drop(['0/1', 'Timestamp'], axis=1, inplace=True)

    tan = dict.fromkeys(["AIT201", "AIT502", "FIT101", "FIT501", "FIT601", "MV101", "MV303", "P205", "PIT502", "AIT202", "AIT503", "FIT201", "FIT502", "LIT101", "MV201", "MV304", "P302", "PIT503", "AIT203", "AIT504", "FIT301", "FIT503", "LIT301", "MV301", "P101", "P602", "AIT501", "DPIT301", "FIT401", "FIT504", "LIT401", "MV302", "P203", "PIT501"])
    for process in tan.keys():
        res_df = pd.read_csv(os.path.join(folder, 'SWaT', "results_moodes", process + "_results.csv"), sep=",", index_col=0)
        tan[process] = Ensemble(name=process)
        subset = res_df.loc[(res_df['Process'] == process)]
        for l, row in subset.iterrows():
            i = l - min(res_df.loc[(res_df['Process'] == process)].index)
            name = os.path.join(folder, 'SWaT', "ta_moodes", process, "ta_" + str(i)+".txt")
            if not os.path.exists(name) or os.stat(name).st_size == 0: continue
            ta = TAG.Automaton(dot_path=name)
            bkpts = dict()
            breakpoints = np.concatenate((-np.inf, min(data[process]), eval(row.Breakpoints), max(data[process]), np.inf), axis=None)
            for b in range(len(breakpoints) - 1):
                bkpts[str(b)] = [breakpoints[b], breakpoints[b + 1]]
            p = Process(name=row.Process,
                        ta=ta,
                        bkpts=bkpts)
            tan[process].automata[i] = p

    return tan
