#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Synthetic data generation
Author: Lenaig Cornanguer (2023)
"""

import os
import random
import numpy as np
import pandas as pd
from scipy import signal

def generate_motif(rng, length):
	sigma = random.uniform(0.1, 0.5)
	ts = list()
	val = 0
	for i in range(length):
		val = random.gauss(val, sigma)
		ts.append(val)
	return ts

def generate(setup="noise_01"):
	random.seed(4321)
	rng = np.random.default_rng(4321)

	path = os.getcwd()

	# MOTIFS GENERATION
	motifs = list()
	nb = 5
	for j in range(nb):
		motifs.append(generate_motif(rng, length=25))
	# motifs_ts = pd.DataFrame({"x": range(len(motifs[0])), "var1": motifs[0], "var2": motifs[1], "var3": motifs[2], "var4": motifs[3], "var5": motifs[4]})
	# motifs_ts.to_csv("./motifs.csv", sep=',',index=False)

	# TIME SERIES GENERATION
	noise = 0.01 if setup == "noise_01" else 0
	time_series = generate_ts(rng, motifs, 1000, noise) # Learning sample
	calib_time_series = generate_ts(rng, motifs, 500, noise) # Calibration sample
	test_time_series = generate_ts(rng, motifs, 750, noise) # Test sample

	test_time_series, truth = add_anomalies(rng, test_time_series)

	into_dict = lambda ts: {"var"+str(i+1): ts[i] for i in range(len(ts))}

	folder = setup
	data_train = pd.DataFrame(data=into_dict(time_series))
	data_train.to_csv(os.path.join(path, folder, "data", "train.csv"), sep=',',index=False)

	data_calib = pd.DataFrame(data=into_dict(calib_time_series))
	data_calib.to_csv(os.path.join(path, folder, "data", "calib.csv"), sep=',',index=False)

	data_test = pd.DataFrame(data=into_dict(test_time_series))
	data_test.to_csv(os.path.join(path, folder, "data", "test.csv"), sep=',',index=False)

	ground_truth = pd.DataFrame(data={"flag": truth})
	ground_truth.to_csv(os.path.join(path, folder, "data", "truth.csv"), sep=',',index=False)

def generate_ts(rng, motifs, length, noise):
	time_series = list()
	for ts in motifs:
		series = list()
		for i in range(length):
			series.append(rng.laplace(ts[i%len(ts)], noise))
		time_series.append(series)
	return time_series

def add_anomalies(rng, test_time_series):
	# Random values on x time units on var2
	t = 100-1 # start
	x = 50 # duration
	ts = 1
	for i in range(t, t+x):
		test_time_series[ts][i] = random.uniform(min(test_time_series[ts]), max(test_time_series[ts]))

	# Slowed pattern on var3
	t = 250-1 # start
	x = 50 # duration
	ts = 2
	sample = np.array(test_time_series[ts][t:t+x])
	sample = signal.resample(sample, int(len(sample)*1.5))
	for i in range(t, t+x):
		test_time_series[ts][i] = sample[i-t]

	# Values multiplied by a factor on var4
	t = 400-1 # start
	x = 50 # duration
	ts = 3
	for i in range(t, t+x):
		test_time_series[ts][i] = test_time_series[ts][i]*1.5

	# Reversed pattern on var5
	t = 550-1 # start
	x = 50 # duration
	ts = 4
	test_time_series[4][t:t+x] = test_time_series[4][t:t+x][::-1]

	truth = [0 if not ((100 - 1 <= i < 150 - 1) or (250 - 1 <= i < 300 - 1) or (400 - 1 <= i < 450 - 1) or (
				550 - 1 <= i < 600 - 1)) else 1 for i in range(len(test_time_series))]

	return test_time_series, truth

