#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Timed automata network initialization with learned automata for the anomaly detection on synthetic data
Author: Lenaig Cornanguer (2023)
"""

import os
import pandas as pd
from AD_with_ensemble import *

def import_tan_mood(setup):
    folder = os.path.join(os.getcwd(), "synthetic_data", setup)
    data = pd.read_csv(os.path.join(folder, "data", "train.csv"), sep=",", decimal=".")
    res_df = pd.read_csv(os.path.join(folder, "moodes_results.csv"), sep=",", index_col=0)

    tan = dict.fromkeys(data.columns)
    for process in tan.keys():
        tan[process] = Ensemble(name=process)
        subset = res_df.loc[(res_df['Process'] == process)]
        for l, row in subset.iterrows():
            i = l - min(res_df.loc[(res_df['Process'] == process)].index)
            filename = os.path.join(folder, "ta_moodes", "ta_" + process +"_"+str(i)+".txt")
            if os.path.exists(filename): 
                ta = TAG.Automaton(dot_path=filename)
            elif eval(row.Breakpoints) == 0:
                ta = TAG.Automaton()
            else: continue
            bkpts = dict()
            breakpoints = np.concatenate((-np.inf, min(data[process]), eval(row.Breakpoints), max(data[process]), np.inf), axis=None)
            for b in range(len(breakpoints) - 1):
                bkpts[str(b)] = [breakpoints[b], breakpoints[b + 1]]
            p = Process(name=row.Process,
                        ta=ta,
                        bkpts=bkpts)
            tan[process].automata[i] = p

    return tan
