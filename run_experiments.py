#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Run the experiment pipeline according to the options
Author: Lenaig Cornanguer (2023)
"""

from MOODES.tss_generation_from_ts import generate_seq_from_ts
from Persist.persist_discretization import discretize_with_persist
from SAX.sax_discretization import discretize_with_sax
from synthetic_data.time_series_generation import generate
from ta_learning import learn_tas
from anomaly_detection import anomaly_detection_synthetic, anomaly_detection_batadal, anomaly_detection_swat

def run(options):
    if options.dataset.value == 0: # SYNTHETIC DATA
        run_synthetic(options)
    elif options.dataset.value == 1: # BATADAL
        run_batadal(options)
    elif options.dataset.value == 2:  # SWaT
        run_swat(options)

def run_batadal(options):
    if options.discretization_method.value in [1, 2]:  # MOODES & TOPSIS
        if options.discretization:
            print('Data discretization...')
            generate_seq_from_ts(dataset="BATADAL")
        if options.ta_learning:
            print('TA learning...')
            learn_tas(dataset="BATADAL")
    elif options.discretization_method.value == 3: # Persist
        discretize_with_persist(options)
    elif options.discretization_method.value == 4: # SAX
        discretize_with_sax(options)
    if options.anomaly_detection:
        print('Anomaly detection...')
        anomaly_detection_batadal(options.discretization_method)

def run_synthetic(options):
    if options.data_generation:
        print('Data without noise generation...')
        generate("noise_0")
        print('Data with noise generation...')
        generate("noise_01")
    if options.discretization:
        print('Data without noise discretization...')
        generate_seq_from_ts(dataset="synthetic_data/noise_0")
        print('Data with noise discretization...')
        generate_seq_from_ts(dataset="synthetic_data/noise_01")
    if options.ta_learning:
        print('TA learning for data without noise...')
        learn_tas(dataset="synthetic_data/noise_0")
        print('TA learning for data with noise...')
        learn_tas(dataset="synthetic_data/noise_01")
    if options.anomaly_detection:
        print('Anomaly detection on data without noise...')
        anomaly_detection_synthetic("noise_0", w_size=3)
        print('Anomaly detection on data with noise...')
        anomaly_detection_synthetic("noise_01")

def run_swat(options):
    if options.discretization:
        print('Data discretization...')
        generate_seq_from_ts(dataset="SWaT")
    if options.ta_learning:
        print('TA learning...')
        learn_tas(dataset="SWaT")
    if options.anomaly_detection:
        print('Anomaly detection...')
        anomaly_detection_swat()

