#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Discretization and ta learning based on SAX representation
Author: Lenaig Cornanguer (2023)
"""
import ast
import os
import numpy as np
import pandas as pd
from pyts.approximation import SymbolicAggregateApproximation, PiecewiseAggregateApproximation
from scipy.stats import norm
from utils_tss import cut_into_seq, summarize
import TAG

def discretize_with_sax(options):
	n_min = options.n_sax[0]
	n_max = options.n_sax[1]
	path = os.getcwd()
	data = pd.read_csv(os.path.join(path, "BATADAL", "data", "BATADAL_dataset03.csv"), dtype={'DATETIME': str}, sep=',', engine='python')
	res_df = pd.DataFrame(columns=["Process", "n", "Breakpoints"])
	if options.discretization:
		print('Data discretization...')
		# SAX
		for process in [c for c in data.columns if c[:2] == "F_" or c[:2] == "P_"]:
			for n_bins in range(n_min, n_max+1):
				# Data standardization
				ts = data[process]
				moy = np.mean(ts)
				std = np.std(ts)
				if std != 0: X_standard = (ts - moy) / std
				else: X_standard = ts
				X_standard = np.array(X_standard).reshape(1, -1)
				# Piecewise Aggregate Approximation
				window_size = 5
				paa = PiecewiseAggregateApproximation(window_size=window_size)
				X_paa = paa.transform(X_standard)
				# Symbolic Aggregate Approximation
				sax = SymbolicAggregateApproximation(n_bins=n_bins, strategy='normal', alphabet=list(range(1,n_bins+1)))
				X_sax = sax.transform(X_paa)

				# Sequences writing
				tss = [v for v in X_sax[0] for i in range(window_size)][:-(window_size - len(ts) % window_size)]
				tss_list = [" ".join(e) for e in cut_into_seq(summarize(tss), 10)]
				with open(os.path.join(path, "SAX", "tss_sax", "tss_" + process + "_" + str(n_bins) + "_sax.txt"), "w") as f:
					f.write('\n'.join(tss_list))
				bkpts = norm.ppf(np.linspace(0, 1, n_bins + 1)[1:-1])
				bkpts = np.concatenate((-np.inf, min(ts), np.sort([b * std + moy for b in bkpts]), max(ts), np.inf), axis=None)
				res_df = pd.concat([res_df, pd.DataFrame({"Process": process, "n": n_bins, "Breakpoints": str(list(bkpts))}, index=[0])])
		res_df['Breakpoints'] = res_df['Breakpoints'].apply(lambda x: ast.literal_eval(x.replace('inf','2e308')))
		res_df.to_csv(os.path.join(path, "SAX", "sax_results.csv"))

	# TA LEARNING
	if options.ta_learning:
		print('TA learning...')
		res_df = pd.read_csv(os.path.join(path, "SAX", "sax_results.csv"), sep=',', engine='python')
		for process in [c for c in data.columns if c[:2] == "F_" or c[:2] == "P_"]:
			for n_bins in range(n_min, n_max+1):
				try:
					l = TAG.TALearner(tss_path=os.path.join(path, "SAX", "tss_sax", "tss_" + process + "_" + str(n_bins) + "_sax.txt"))
					mem = l.ta.print(reduced_guard=False, gtime=False)
					with open(os.path.join(path, "SAX", "ta_sax", "ta_sax_"+process+"_"+str(n_bins)+".txt"), 'w') as f:
						f.write('\n'.join(mem))
				except:
					continue
