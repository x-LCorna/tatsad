#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Util functions for the generation of event sequences
Author: Lenaig Cornanguer (2023)
"""

def summarize(x):
    ts = ""
    cpt = 0
    s = None
    for i in range(len(x)):
        if s == x[i]:
            cpt += 1
        else:
            s = x[i]
            ts += str(int(s)) + ":" + str(cpt) + " "
            cpt = 1
    return ts[:-1]


def cut_into_seq(ts, length):
    mem = list()
    pairs = ts.split(' ')
    for i in range(len(pairs)):
        first = pairs[i].split(':')
        i_fin = min(i + length + 1, len(pairs))
        mem.append([first[0] + ':0'] + pairs[i + 1:i_fin])
    return mem
