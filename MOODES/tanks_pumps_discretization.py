#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Time series discretization for BATADAL tank level and actuator status (based on knowledge given in the challenge)
Author: Lenaig Cornanguer (2023)
"""

import os
import pandas as pd
import TAG

class Discretizer:
    # Simplified version of Detector to discretize tank level and actuator status simultaneously
	# Produce event sequences for interacting component only
    def __init__(self, tan, data):
        self.i = 0
        self.tan = tan
        self.data = data

    def read_data(self):
        events = dict.fromkeys(self.tan.keys())
        row = self.data.iloc[self.i]
        for process in self.tan.keys():
            events[process] = self.tan[process].get_event(row[process])
        return events

    def main(self):
        tanks = [name for name, process in tan.items() if isinstance(process, Tank)]
        while self.i < len(self.data):
            data = self.read_data()
            for name in tanks:
                self.update_interacting_process(name, data)
            self.i += 1

    def update_interacting_process(self, name, events):
        tank = self.tan[name]
        event_seq = tank.get_event_seq(events[name])
        intermediate = list()
        for actuator in tank.actuators:
            if events[actuator.name] != actuator.current_event and actuator.current_event is not None:
                synchro = events[actuator.name][:-1] + "!"
                if synchro not in event_seq and tank.allow_synchro(synchro, self.data[name].iloc[self.i]):
                    intermediate.append(synchro)
        if len(intermediate) > 0: event_seq = tank.get_event_seq(events[name], intermediate)
        for actuator in tank.actuators:
            self.update_actuator(actuator.name, events)
        self.update_tank(name, event_seq)

    def update_tank(self, name, event_seq):
        tank = self.tan[name]
        if tank.current_event is None:
            tank.update(event_seq[0])
            return
        for i in range(1, len(event_seq)):
            edge = tank.ta.next_edge(tank.current_state.name, event_seq[i], tank.clock)
            tank.update(event_seq[i])
            tank.clock -= 1
        tank.clock += 1

    def update_actuator(self, name, events):
        actuator = self.tan[name]
        if actuator.current_event is not None and events[name][:-1] == actuator.current_event[:-1]:  # No transition
            actuator.clock += 1
        else:  # Transition
            edge = actuator.ta.next_edge(actuator.current_state.name, events[name], actuator.clock)
            actuator.update(events[name])


class Process:
    def __init__(self, name, ta, bkpts):
        self.name = name
        self.ta = ta
        self.clock = 0
        self.current_state = [s for s in ta.states if s.initial][0]
        self.current_event = None
        self.bkpts = bkpts
        self.seq = list()

    def reinit(self):
        self.current_event = None
        self.clock = 0
        self.current_state = [s for s in self.ta.states if s.initial][0]

    def update(self, event):
        # Write the event sequence
        self.seq.append(str(event) + ":" + str(self.clock))
        self.clock = 1
        self.current_event = event

    def get_event(self, value):
        for k, v in self.bkpts.items():
            if v[0] <= value < v[1]:
                return str(k)


class Tank(Process):
    def __init__(self, name, ta, bkpts, actuators, synchros):
        super().__init__(name, ta, bkpts)
        self.actuators = actuators
        self.synchros = synchros

    def get_synchro(self, event, current_event=None):
        if current_event is None: current_event = self.current_event
        for k, v in self.synchros.items():
            if k[0] == current_event and k[1] == event:
                return v["synchro"]

    def allow_synchro(self, synchro, value):
        # Exceeding a threshold can be masked if it occurs in less than an hour and right after it goes below it.
		# If we have seen an actuator status change, it means that the threshold was masked.
		# Therefore, we add this masked change to the sequence of event in the training set.
        for k, v in self.synchros.items():
            if synchro == v["synchro"]:
                v["tol"][0] = min(value, v["tol"][0])
                v["tol"][1] = max(value, v["tol"][1])
        return True

    def get_event_seq(self, event, intermediate=None):
        if self.current_event is None: return [event]
        event_seq = list()
        i_event = self.get_index(event)
        i_current_event = self.get_index(self.current_event)
        if intermediate is None:
            if i_current_event == i_event: return [event]
            indexes = list(range(i_current_event, i_event + self.order(i_current_event, i_event),
                                 self.order(i_current_event, i_event)))
            event_seq = self.indexes_to_events(indexes)
        else:
            for synchro in intermediate:
                t = [k for k, v in self.synchros.items() if v['synchro'] == synchro][0]
                i_1 = self.get_index(t[0])
                i_2 = self.get_index(t[1])
                event_seq += self.indexes_to_events(list(
                    range(i_current_event, i_1 + self.order(i_current_event, i_1), self.order(i_current_event, i_1))))
                event_seq.append(synchro)
                event_seq += self.indexes_to_events(
                    list(range(i_2, i_event + self.order(i_2, i_event), self.order(i_2, i_event))))
        return event_seq

    def order(self, a, b):
        if a > b:
            return -1
        elif a <= b:
            return 1

    def get_index(self, event):
        i = 0
        for k, v in self.bkpts.items():
            if k == event: return i
            i += 1

    def indexes_to_events(self, indexes):
        event_seq = list()
        for i in range(len(indexes) - 1):
            event_seq.append(list(self.bkpts.keys())[indexes[i]])
            synchro = self.get_synchro(list(self.bkpts.keys())[indexes[i + 1]], list(self.bkpts.keys())[indexes[i]])
            if synchro is not None: event_seq.append(synchro)
        event_seq.append(list(self.bkpts.keys())[indexes[-1]])
        return event_seq


class Actuator(Process):
    def __init__(self, name, ta, bkpts):
        super().__init__(name, ta, bkpts)

    def get_event(self, value):
        for k, v in self.bkpts.items():
            if value == v: return k

def cut_into_seq(ts, length, actuator):
    mem = list()
    pairs = ts.split(' ')
    for i in range(len(pairs)):
        first = pairs[i].split(':')
        if not actuator:
            if first[0][-1] == '?' or first[0][-1] == '!': continue
        i_fin = min(i + length + 1, len(pairs))
        mem.append([first[0] + ':0'] + pairs[i + 1:i_fin])
    return mem

# Knowledge (activation settings of the PLC)
s_v2 = Actuator(name="S_V2",
                ta=TAG.Automaton(),
                bkpts={"openV2?": 1, "closeV2?": 0})
s_pu1 = Actuator(name="S_PU1",
                 ta=TAG.Automaton(),
                 bkpts={"openPU1?": 1, "closePU1?": 0})
s_pu2 = Actuator(name="S_PU2",
                 ta=TAG.Automaton(),
                 bkpts={"openPU2?": 1, "closePU2?": 0})
s_pu3 = Actuator(name="S_PU3",
                 ta=TAG.Automaton(),
                 bkpts={"openPU3?": 1, "closePU3?": 0})
s_pu4 = Actuator(name="S_PU4",
                 ta=TAG.Automaton(),
                 bkpts={"openPU4?": 1, "closePU4?": 0})
s_pu5 = Actuator(name="S_PU5",
                 ta=TAG.Automaton(),
                 bkpts={"openPU5?": 1, "closePU5?": 0})
s_pu6 = Actuator(name="S_PU6",
                 ta=TAG.Automaton(),
                 bkpts={"openPU6?": 1, "closePU6?": 0})
s_pu7 = Actuator(name="S_PU7",
                 ta=TAG.Automaton(),
                 bkpts={"openPU7?": 1, "closePU7?": 0})
s_pu8 = Actuator(name="S_PU8",
                 ta=TAG.Automaton(),
                 bkpts={"openPU8?": 1, "closePU8?": 0})
s_pu9 = Actuator(name="S_PU9",
                 ta=TAG.Automaton(),
                 bkpts={"openPU9?": 1, "closePU9?": 0})
s_pu10 = Actuator(name="S_PU10",
                  ta=TAG.Automaton(),
                  bkpts={"openPU10?": 1, "closePU10?": 0})
s_pu11 = Actuator(name="S_PU11",
                  ta=TAG.Automaton(),
                  bkpts={"openPU11?": 1, "closePU11?": 0})

l_t1 = Tank(name="L_T1",
            ta=TAG.Automaton(),
            bkpts={"1": [0, 0.320111841], "2": [0.320111841, 1], "3": [1, 4], "4": [4, 4.5], "5": [4.5, 6.3],
                   "6": [6.3, 6.5]},
            actuators=[s_pu1, s_pu2],
            synchros={("4", "3"): {"synchro": "openPU1!", "tol": [0.509729922, 0.509729922]}, # tol = [min(observed, given_min), max(observed, real_max)]
                      ("5", "6"): {"synchro": "closePU1!", "tol": [0, 0]},
                      ("3", "2"): {"synchro": "openPU2!", "tol": [0.509729922, 1.117213607]},
                      ("4", "5"): {"synchro": "closePU2!", "tol": [4.237445354, 4.66071701]}})

l_t2 = Tank(name="L_T2",
            ta=TAG.Automaton(),
            bkpts={"1": [0, 0.293875813], "2": [0.293875813, 0.5], "3": [0.5, 5.5], "4": [5.5, 5.663802147],
                   "5": [5.663802147, 5.9]},
            actuators=[s_v2],
            synchros={("3", "4"): {"synchro": "closeV2!", "tol": [4.710795403, 5.663802147]},
                      ("3", "2"): {"synchro": "openV2!", "tol": [0.293875813, 2.049002886]}})

l_t3 = Tank(name="L_T3",
            ta=TAG.Automaton(),
            bkpts={"1": [0, 1], "2": [1, 3], "3": [3, 3.5], "4": [3.5, 5.3], "5": [5.3, 5.433021545],
                   "6": [5.433021545, 6.75]},
            actuators=[s_pu4, s_pu5],
            synchros={("3", "2"): {"synchro": "openPU4!", "tol": [2.882846594, 3.501587152]},
                      ("4", "5"): {"synchro": "closePU4!", "tol": [4.848326206, 5.433021545]},
                      ("2", "1"): {"synchro": "openPU5!", "tol": [0, 0]},
                      ("3", "4"): {"synchro": "closePU5!", "tol": [3.191145182, 3.191145182]}})

l_t4 = Tank(name="L_T4",
            ta=TAG.Automaton(),
            bkpts={"1": [0, 2], "2": [2, 3], "3": [3, 3.5], "4": [3.5, 4.5], "5": [4.5, 4.7]},
            actuators=[s_pu6, s_pu7],
            synchros={("2", "1"): {"synchro": "openPU6!", "tol": [2.006145, 2.510221481]},
                      ("3", "4"): {"synchro": "closePU6!", "tol": [2.792634249, 3.700614214]},
                      ("3", "2"): {"synchro": "openPU7!", "tol": [2.403930187, 4.021317959]},
                      ("4", "5"): {"synchro": "closePU7!", "tol": [3.009527445, 4.690665245]}})

l_t5 = Tank(name="L_T5",
            ta=TAG.Automaton(),
            bkpts={"1": [0, 1.293241262], "2": [1.293241262, 1.5], "3": [1.5, 4], "4": [4, 4.162725925],
                   "5": [4.162725925, 4.5]},
            actuators=[s_pu8],
            synchros={("3", "2"): {"synchro": "openPU8!", "tol": [1.293241262, 2.656090975]},
                      ("3", "4"): {"synchro": "closePU8!", "tol": [3.189588547, 4.162725925]}})

l_t7 = Tank(name="L_T7",
            ta=TAG.Automaton(),
            bkpts={"1": [0, 1], "2": [1, 2.5], "3": [2.5, 3], "4": [3, 4.8], "5": [4.8, 4.999618053],
                   "6": [4.999618053, 5]},
            actuators=[s_pu10, s_pu11],
            synchros={("3", "2"): {"synchro": "openPU10!", "tol": [1.560883403, 3.669429064]},
                      ("4", "5"): {"synchro": "closePU10!", "tol": [2.862300158, 4.999618053]},
                      ("2", "1"): {"synchro": "openPU11!", "tol": [1.175509572, 1.227926254]},
                      ("3", "4"): {"synchro": "closePU11!", "tol": [1.562320828, 4.055660725]}})

tan = {"S_PU1": s_pu1, "S_PU2": s_pu2, "S_PU3": s_pu3, "S_PU4": s_pu4, "S_PU5": s_pu5, "S_PU6": s_pu6,
	   "S_PU7": s_pu7, "S_PU8": s_pu8, "S_PU9": s_pu9, "S_PU10": s_pu10, "S_PU11": s_pu11, "S_V2": s_v2,
	   "L_T1": l_t1, "L_T2": l_t2, "L_T3": l_t3, "L_T4": l_t4, "L_T5": l_t5, "L_T7": l_t7}

path = os.path.dirname(os.getcwd())
data = pd.read_csv(os.path.join(path, "data", "BATADAL_dataset03.csv"), dtype={'DATETIME':str})

discretizer = Discretizer(tan, data)
discretizer.main()

for name, process in tan.items():
    ts = ' '.join(discretizer.tan[name].seq)
    tss_list = cut_into_seq(ts, 10, isinstance(process, Actuator))
    tss = [' '.join(e) for e in tss_list]
    with open(os.path.join(path, "MOODES", "tss_moodes/", "tss_" + name +".txt"), "w") as f:
        f.write('\n'.join(tss))
