"""
Multi-Objective Optimization Discretization
Author: 
"""

import numpy as np
import pandas as pd
from pymoo.core.problem import Problem
from pymoo.core.mutation import Mutation
import math
from scipy.stats import wasserstein_distance

class MOODES(Problem):

    def __init__(self, ts, kmin=None, kmax=None, testdata=None, y_train=None, y_test=None):
        self.ts = ts
        self.testdata = testdata
        self.y_test = y_test
        self.y_train = y_train
        xl = np.min(ts)
        xu = np.max(ts)
        self.kmin = 2 if not kmin else kmin
        self.kmax = len(ts) if not kmax else kmax
        self.mem = pd.DataFrame(columns=['gen', 'acc', 'bkpts', 'k', 'ssd', 'persistence'])
        n_var = kmax-1
        super().__init__(n_var, n_obj=3, n_constr=0, xl=xl, xu=xu)

    def _evaluate(self, x, out, *args, **kwargs):
        f1_lim = (self.kmin, self.kmax)
        f2_lim = (0, self.ssd(np.full(shape=self.ts.shape, fill_value=0)))
        f3_lim = (-1, 1)
        f1 = np.apply_along_axis(lambda i: np.count_nonzero(~np.isnan(i))+1, 1, x) # k
        tmp = np.apply_along_axis(self.symbolize, 1, x) # SSD
        tmp = np.reshape(tmp, (tmp.shape[0], tmp.shape[2] * tmp.shape[1]))
        f2 = np.apply_along_axis(self.ssd, 1, tmp) # SSD
        sym = np.apply_along_axis(self.symbolize, 1, x) # Persistence
        tmp = np.apply_along_axis(lambda x: np.append(x, np.nan), 2, sym)
        tmp = np.reshape(tmp, (tmp.shape[0], tmp.shape[2] * tmp.shape[1]))
        f3 = np.apply_along_axis(self.persistence, 1, tmp) # Persistence
        out["F"] = np.column_stack([(f1-f1_lim[0])/(f1_lim[1] - f1_lim[0]), (f2-f2_lim[0])/(f2_lim[1] - f2_lim[0]), -((f3-f3_lim[0])/(f3_lim[1] - f3_lim[0]))])

    def persistence(self, s_ts):
        n = s_ts.shape[0]
        s_ts = s_ts.reshape(-1, 1)
        k = int(max([e[0] for e in s_ts])+1)
        a = np.zeros(shape=(k, k), dtype=float)
        p = sum(np.tile(s_ts, (1, k)) == np.tile(np.array(range(k)), (n, 1)))/n
        obstup = np.concatenate((np.array(s_ts[0:n-1]), s_ts[1: n]), axis=1)
        t, occ = np.unique(obstup, return_counts=True, axis=0)
        for i in range(t.shape[0]):
            if np.isnan(t[i][0]) or np.isnan(t[i][1]): continue
            a[int(t[i, 0]), int(t[i, 1])] = occ[i]
        a = a/np.tile(np.reshape(np.sum(a, 1), (k, 1)), (1, a.shape[1]))
        st = np.diag(a)
        pscore = np.mean((-1) ** (st < np.transpose(p)) * self.wasserstein(
            np.concatenate((st.reshape(-1, 1), 1 - st.reshape(-1, 1)), axis=1),
            np.concatenate([p.reshape(-1, 1), 1 - p.reshape(-1, 1)], axis=1)))
        return pscore

    def wasserstein(self, p, q):
        res = np.zeros(shape=len(p))
        for i in range(len(p)):
            if np.isnan(p[i][0]) or np.isnan(q[i][0]): res[i] = 0
            else:
                res[i] = wasserstein_distance(np.array([0, 1]), np.array([0, 1]), p[i], q[i])
        return res

    def symbolize(self, breakpoints):
        y = np.zeros(shape=self.ts.shape)
        breakpoints = np.concatenate((-math.inf, np.sort(breakpoints), math.inf), axis=None)
        for i in range(len(breakpoints)-1):
            y[np.nonzero(np.logical_and(self.ts>=breakpoints[i], self.ts<breakpoints[i+1]))] = i
        return y

    def ssd(self, s_ts):
        symbols = np.unique(s_ts)
        ssd = 0
        ts = self.ts.ravel()
        s_ts = s_ts.ravel()
        for s in symbols:
            centroid = np.mean(ts[np.nonzero(s_ts == s)])
            m = ts[np.nonzero(s_ts == s)]
            ssd += np.sum((np.full(m.shape, centroid) - m)**2)
        return ssd

class mutation_bkpts(Mutation):
    def __init__(self):
        super().__init__()

    def _do(self, problem, X, **kwargs):
        def transfo(i):
            i = np.sort(i)
            r = np.random.random()
            if r < 0.1 and np.count_nonzero(~np.isnan(i)) < len(i):
                while(True):
                    nb = np.random.uniform(problem.xl[0], problem.xu[0])
                    if nb not in i:
                        i[-1] = nb
                        break
            elif r < 0.2 and np.count_nonzero(~np.isnan(i)) > 3:
                nnl = np.count_nonzero(~np.isnan(i))
                i[np.random.randint(0, nnl)] = np.nan
            else:
                nnl = np.count_nonzero(~np.isnan(i))
                i[np.random.randint(0, nnl, 1)[0]] = np.random.laplace(i[np.random.randint(0, nnl, 1)[0]], 0.4)
            return i
        np.apply_along_axis(transfo, 1, X)
        return X

