#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Discretization with MOODES and event sequence generation from time series data
Author: Lenaig Cornanguer (2023)
"""

import ast
import os
import numpy as np
import pandas as pd
from pymoo.core.population import Population
from pymoo.algorithms.moo.nsga3 import NSGA3
from pymoo.util.ref_dirs import get_reference_directions
from pymoo.optimize import minimize
import kmeans1d
from pymoo.termination.robust import RobustTermination
from pymoo.termination.ftol import MultiObjectiveSpaceTermination
from MOODES.MOODES import MOODES, mutation_bkpts

from utils_tss import summarize, cut_into_seq

def ssd(ts, s_ts):
    s_ts = s_ts.reshape(-1, 1)
    symbols = np.unique(s_ts)
    ssd = 0
    for s in symbols:
        centroid = np.mean(ts[np.nonzero(s_ts == s)[0]])
        ssd += np.sum((ts[np.nonzero(s_ts == s)[0]] - centroid)**2)
    return ssd

def inject_none(x, kmin):
    p = np.random.random()
    n = max(len(x) - round(p * len(x)), kmin)
    return np.append(np.random.choice(x, n), (len(x) - n) * [np.nan])

def distance(a1, a2):
    return (((a1[0] - a2[0])**2 + (a1[1] - a2[1])**2 + (a1[2] - a2[2])**2)**0.5)

def population_init(ts, kmin, kmax, n):
    # Population initialization
    pop = list()
    for k in range(kmin, kmax+1):
        c = np.array(kmeans1d.cluster(ts, k)[0])
        if len(np.unique(c)) != k: continue
        b = np.full(kmax-1, np.nan)
        for s in range(1, k):
            b[s-1] = (np.min(ts[np.where(c==s)]) + np.max(ts[np.where(c==s-1)]))/2
        pop.append(b)
        b = np.full(kmax-1, np.nan)
        for s in range(1, k):
            b[s-1] = (np.mean(ts[np.where(c==s)]) + np.mean(ts[np.where(c==s-1)]))/2
        pop.append(b)
        b = np.full(kmax-1, np.nan)
        for s in range(1, k):
            b[s-1] = (np.max(ts[np.where(c==s)]) + np.min(ts[np.where(c==s-1)]))/2
        pop.append(b)

    X = np.random.uniform(np.min(ts), np.max(ts), (n - len(pop), kmax-1))
    X = np.apply_along_axis(inject_none, 1, X, kmin)

    pop = Population.new("X", np.concatenate((X, pop)))

    return pop

def find_discretization_solutions(ts, kmin, kmax, n):
    """
    Run the generatic algorithm to find discretization solutions for a given time series
    :param ts: time series to discretize
    :param kmin: minimal number of discrete event
    :param kmax: maximal number of discrete event
    :param n: population size
    :return: a dataframe containing the discretization solutions
    """
    # initialize the population
    pop = population_init(ts, kmin, kmax, n)

    # search optimal solutions
    problem = MOODES(np.reshape(ts, (1, -1)), kmin, kmax)
    ref_dirs = get_reference_directions("das-dennis", 3, n_partitions=12)
    termination = RobustTermination(
        MultiObjectiveSpaceTermination(tol=0.005, n_skip=5), period=20)
    algorithm = NSGA3(pop_size=n, mutation=mutation_bkpts(), sampling=pop, ref_dirs=ref_dirs)
    res = minimize(problem,
                   algorithm,
                   termination=termination,
                   seed=100,
                   verbose=True)

    # add last generation solutions to memory
    f1_lim = (kmin, kmax)
    f2_lim = (0, res.problem.ssd(np.full(shape=res.problem.ts.shape, fill_value=0)))
    f3_lim = (-1, 1)
    for ind in res.algorithm.opt:
        f1 = ind.F[0] * (f1_lim[1] - f1_lim[0]) + f1_lim[0]
        f2 = ind.F[1] * (f2_lim[1] - f2_lim[0]) + f2_lim[0]
        f3 = (-ind.F[2] * (f3_lim[1] - f3_lim[0]) + f3_lim[0])
        res.problem.mem = pd.concat([res.problem.mem, pd.DataFrame({'gen': 'last', 'acc': None, 'bkpts': str([e for e in np.sort(ind.X) if not np.isnan(e)]),
             'k': f1, 'ssd': f2, 'persistence': f3}, index=[0])])
    res.problem.mem['bkpts'] = res.problem.mem['bkpts'].apply(lambda x: ast.literal_eval(x))
    return res.problem.mem

def scale_objectives(df):
    df["k_norm"] = (df["k"]-min(df["k"]))/(max(df["k"])-min(df["k"]))
    df["ssd_norm"] = (df["ssd"]-min(df["ssd"]))/(max(df["ssd"])-min(df["ssd"]))
    df["persistence_norm"] = (df["persistence"]-min(df["persistence"]))/(max(df["persistence"])-min(df["persistence"]))
    return df

def compute_tradeoff(df):
    df["dt"] = 0
    for i in range(len(df)):
        # Trade-off ranking
        d = 0
        if df["gen"].iloc[i] != max(df.gen): continue
        for i2 in range(len(df)):
            if (i == i2) or (df["gen"].iloc[i] != max(df.gen)): continue
            d += distance([df.k_norm.iloc[i], df.ssd_norm.iloc[i], df.persistence_norm.iloc[i]],
                          [df.k_norm.iloc[i2], df.ssd_norm.iloc[i2], df.persistence_norm.iloc[i2]])
        df["dt"].loc[i] = d
    return df
def compute_topsis(df):
    df["topsis"] = 0
    for i in range(len(df)):
        # TOPSIS
        df["topsis"].loc[i] = distance([df.k_norm.iloc[i], df.ssd_norm.iloc[i], df.persistence_norm.iloc[i]], [0, 0, 0])
    return df

def include_to_global_results(df, res_df, process):
    process_df_selected = df[["bkpts", "ssd", "persistence", "k", "dt", "topsis"]]  # Select the columns you want
    process_df_selected = process_df_selected.rename(
        columns={"bkpts": "Breakpoints", "ssd": "SSD", "persistence": "Persistence", "k": "k"})  # Rename the columns
    process_df_selected["Process"] = process
    res_df = pd.concat([res_df, process_df_selected], ignore_index=True)
    return res_df

def seq_generation(res_df, data, export_path, reimported=False):
    # Sequences generation for TA learning with TAG
    s_index = 0
    for l, row in res_df.iterrows():
        if row.Process != res_df.iloc[l-1].Process: s_index = l
        ts = np.array(data[row.Process])
        if reimported: bkpts = eval(row.Breakpoints)
        else: bkpts = row.Breakpoints #eval(row.Breakpoints) #If res_df is reimported, necessity to eval list
        s = np.zeros(shape=ts.shape)
        breakpoints = np.concatenate((-np.inf, min(ts), np.sort([b for b in bkpts if not np.isnan(b)]), max(ts), np.inf), axis=None)
        for i in range(len(breakpoints) - 1):
            s[np.nonzero(np.logical_and(ts >= breakpoints[i], ts < breakpoints[i + 1]))] = str(int(i))
        tss_list = [" ".join(e) for e in cut_into_seq(summarize(s), 10)]
        index = l - s_index
        filename = "tss_" + row.Process + "_" + str(index)
        with open(os.path.join(export_path, filename+".txt"), "w") as f:
            f.write('\n'.join(tss_list))

def generate_seq_from_ts(dataset="BATADAL", n=500, kmin=2, kmax=20):

    path = os.getcwd()

    res_df = pd.DataFrame(columns=["Process", "Breakpoints", "SSD", "Persistence", "k", "dt", "topsis"])

    if dataset == "BATADAL":
        data = pd.read_csv(os.path.join(path, dataset, "data", "BATADAL_dataset03.csv"), dtype={'DATETIME': str})
        processes = ['F_PU1', 'F_PU2', 'F_PU4', 'F_PU6', 'F_PU7', 'F_PU8', 'F_PU10', 'F_PU11', 'F_V2',
                     'P_J269', 'P_J300', 'P_J256', 'P_J289', 'P_J415',
                     'P_J302', 'P_J306', 'P_J307', 'P_J317', 'P_J14', 'P_J422']
    elif dataset == "SWaT":
        data = pd.read_csv(os.path.join(path, dataset, "data", "Physical", "SWaT_Dataset_Normal_v1.csv"), sep=";", decimal=",")
        data = data[100000:400000] # 100500] for quick test
        processes = ["FIT101", "LIT101", "MV101", "FIT201", "MV201", "DPIT301", "FIT301", "LIT301", "MV301", "MV302", "MV303", "MV304", "FIT401", "LIT401", "UV401", "FIT501", "FIT502", "FIT503", "FIT504", "PIT502", "PIT503", "FIT601"]
    else:
        data = pd.read_csv(os.path.join(path, dataset, "data", "train.csv"), sep=",", decimal=".")
        processes = data.columns

    # MOOD for each process
    for process in processes: # [:1] for quick test
        print(process)
        ts = np.array(data[[process]])
        df = find_discretization_solutions(ts, kmin, kmax, n)
        df = scale_objectives(df)
        df = compute_topsis(df)
        df = compute_tradeoff(df)
        res_df = include_to_global_results(df, res_df, process)
        if dataset == "SWaT":
            res_df.to_csv(os.path.join(path, dataset, "results_moodes", process + "_results.csv"))
        else:
            res_df.to_csv(os.path.join(path, dataset, "moodes_results.csv")) # save current results

    # event sequences writing
    if dataset == "SWaT":
        for process in processes:
            res_df = pd.read_csv(os.path.join(path, "SWaT", "results_moodes", process + "_results.csv"))
            seq_generation(res_df, data, export_path=os.path.join(path, dataset, "tss_moodes"), reimported=True)
    else:
        seq_generation(res_df, data, export_path=os.path.join(path, dataset, "tss_moodes"))

