#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Options class for experiment parameters
Author: Lenaig Cornanguer (2023)
"""

import os
import warnings
from enum import Enum

class Discretization(Enum):
    MOODES = 1
    TOPSIS = 2
    PERSIST = 3
    SAX = 4

    def __str__(self):
        if self.value == 1:
            return 'MOODES'
        elif self.value == 2:
            return 'TOPSIS'
        elif self.value == 3:
            return 'Persist'
        elif self.value == 4:
            return 'SAX'

class Dataset(Enum):
    SYNTHETIC = 0
    BATADAL = 1
    SWAT = 2

    def __str__(self):
        if self.value == 0:
            return 'synthetic_data'
        elif self.value == 1:
            return 'BATADAL'
        elif self.value == 2:
            return 'SWaT'

class Options():
    def __init__(self, dataset="BATADAL", discretization_method="MOODES", discretization=False, ta_learning=False, anomaly_detection=True, data_generation=False, detailed=False, n_sax=[3, 3]):
        """
        Experiment options
        :param dataset: [synthetic_data/BATADAL/SWaT]
        :param discretization_method: [MOODES/TOPSIS/Persist/SAX]
        :param discretization: True to perform the discretization step
        :param ta_learning: True to perform the TA learning step
        :param anomaly_detection: True to perform the anomaly detection step
        :param data_generation: True to perform the data generation step
        :param detailed: True to get the detailed anomaly detection results analysis (BATADAL dataset only)
        """
        self.dataset = self.get_dataset(dataset)
        self.discretization_method = self.get_discretization_method(discretization_method)
        self.discretization = discretization
        self.ta_learning = ta_learning
        self.anomaly_detection = anomaly_detection
        self.data_generation = data_generation
        self.detailed = detailed
        self.n_sax = n_sax # Number of bins for SAX

        self.check_values()

    def get_dataset(self, input):
        if input in ('BATADAL', 'Batadal', 'batadal'):
           return Dataset.BATADAL
        elif input in ('SWaT', 'SWAT', 'swat'):
           return Dataset.SWAT
        else: # synthetic data
            return Dataset.SYNTHETIC

    def get_discretization_method(self, input):
        if input in ('Persist', 'persist'):
           return Discretization.PERSIST
        elif input in ('SAX', 'sax'):
           return Discretization.SAX
        elif input in ('TOPSIS', 'topsis'):
           return Discretization.TOPSIS
        else: # MOODES
            return Discretization.MOODES

    def check_values(self):
        if self.dataset.value != 1 and self.discretization_method.value != 1:
            raise NotImplementedError('Data discretization with SAX/Persist/MOODES+TOPSIS only implemented for BATADAL dataset here.')

        if self.data_generation and self.dataset.value != 0:
            warnings.warn('Data generation only for synthetic dataset. Data generation set to False.')
        self.data_generation = False

        if self.detailed and self.dataset.value != 1:
            warnings.warn('Detailed anomaly detection analysis only for BATADAL. Detailed set to False.')
        self.detailed = False

        if self.discretization_method.value == 2 and (self.discretization or self.ta_learning):
            warnings.warn('Please note that the TOPSIS selection solution is applied to MOODES results. Hence, this parameter will only be taken into account for anomaly detection part.')

        if self.discretization and self.discretization_method.value in [1, 2]:
            warnings.warn('Re-running MOODES will lead to different discretization solutions. The TA should be relearned to correspond to the new discretization results.')
            self.ask_for_ta_deletion()


    def ask_for_ta_deletion(self):
        print('Delete outdated timed automata and event sequences? [Y/n]', end=' ')
        rep = input()
        if rep != 'n':
            folders = [os.path.join(str(self.dataset), 'ta_moodes'), os.path.join(str(self.dataset), 'tss_moodes'), os.path.join(str(self.dataset), 'noise_0', 'tss_moodes'), os.path.join(str(self.dataset), 'noise_01', 'tss_moodes')]
            for folder in folders:
                os.system('rm -rf ' + folder + '/*')
        if not self.ta_learning:
            print('Relearn the timed automata? [Y/n]', end=' ')
            rep = input()
            if rep != 'n':
                self.ta_learning = True
